package dev.evans.services;

import dev.evans.bankapp.BankAppSession;
import dev.evans.daos.LoginDAO;
import dev.evans.daos.LoginDAOImpl;
import dev.evans.models.User;
import dev.evans.models.UserRole;

public class LoginServiceImpl implements LoginService {
	
	private LoginDAO loginDAO;

	public LoginServiceImpl() {
		super();
		loginDAO = new LoginDAOImpl();
	}

	@Override
	public boolean verifyUserCredentials(String username, String password, UserRole userRole) {
		if(username != null && password != null && userRole != null) { // all parameters cannot be null to continue
			username = username.toUpperCase();
			// first check if the credentials match a user in the database
			User loginAttempt = loginDAO.getUserByCredentials(username, password);
			if(loginAttempt != null)
			{
				// then check if the user has the privileges to log into the current service portal
				UserRole attemptedRole = loginAttempt.getUserRole();
				if(attemptedRole.compareTo(userRole) >= 0) {
					BankAppSession.setLoggedInUser(loginAttempt);
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void deleteLoggedInUser() {
		BankAppSession.setLoggedInUser(null); // deletes the logged-in user currently stored in memory
	}

}
