package dev.evans.frontends;

import java.util.List;

import dev.evans.bankapp.BankAppSession;
import dev.evans.models.Account;
import dev.evans.models.AccountType;
import dev.evans.services.CustomerService;
import dev.evans.services.CustomerServiceImpl;
import dev.evans.utils.IOUtil;

public class CustomerFrontendImpl implements CustomerFrontend {
	
	// reference to customer service component
	private CustomerService customerService;

	public CustomerFrontendImpl() {
		super();
		customerService = new CustomerServiceImpl();
	}

	@Override
	public void showCustomerMenu() {
		boolean isLoggedIn = true; // indicates if the user is logged in
		int menuSelection; // stores the user's numeric menu choice

		do {
			IOUtil.clearScreen();

			// display customer services menu options
			System.out.println("-----------------------------");
			System.out.println("| My Bank Customer Services |");
			System.out.println("-----------------------------\n");

			System.out.println("Logged in user: " + BankAppSession.getLoggedInUser().getFullName() + "\n");

			System.out.println("Please select a banking service to perform,");
			System.out.println("or select [5] to log out.\n");
			System.out.println("   [1] Apply for a new account");
			System.out.println("   [2] View your active accounts");
			System.out.println("   [3] Withdraw funds from an account");
			System.out.println("   [4] Deposit funds into an account");
			System.out.println("   [5] Log out\n");

			// get a customer service choice from the user
			menuSelection = IOUtil.readMenuChoice(5);

			IOUtil.clearScreen();

			// choice a service based on user's menu choice
			switch(menuSelection) {
			case 1:
				applyForAccount();
				break;
			case 2:
				viewAccountBalance();
				break;
			case 3:
				withdrawFromAccount();
				break;
			case 4:
				depositIntoAccount();
				break;
			case 5:
				isLoggedIn = false;
			}

		} while(isLoggedIn); // re-display menu while user is still logged in
	}

	@Override
	public void applyForAccount() {
		int typeSelection; // stores the user's account choice

		// prompt user for account type
		System.out.println("What type of account are you applying for?\n");
		System.out.println("   [1] Checking");
		System.out.println("   [2] Savings\n");

		// get an account type choice from the user
		typeSelection = IOUtil.readMenuChoice(2);

		double startingBalance; // account's starting balance
		
		System.out.println();

		// get starting balance from user; $250,000 is the max starting balance limit
		String prompt = "Enter the starting balance for the new account: ";

		startingBalance = IOUtil.readDollarAmount(prompt);

		// add the new pending account; will fail if user has another account already pending
		boolean success = customerService.addNewPendingAccount(typeSelection == 1 ? AccountType.CHECKING : AccountType.SAVINGS, startingBalance);
		if(success) {
			System.out.println("\nApplication submitted and awaiting approval by a bank administrator.\n");
		}
		else {
			System.out.println("\nApplication denied. Unable to submit a new application while another");
			System.out.println("pending account is awaiting approval by a bank administrator.\n");
		}

		IOUtil.promptEnterKey();
	}

	@Override
	public void viewAccountBalance() {
		System.out.println("Account details for " + BankAppSession.getLoggedInUser().getFullName() + "\n");
		List<Account> accounts = customerService.getActiveAccounts();
		// if active accounts exist, display each account owned by the logged in user
		if(accounts.size() > 0) {
		IOUtil.printAccountHeader();
		for(int i = 0; i < accounts.size(); i++) {
			System.out.println(accounts.get(i).getFullDetails());
		}
		System.out.println();
		}
		else {
			System.out.println("User currently has no active accounts.\n");
		}
		
		IOUtil.promptEnterKey();
	}

	@Override
	public void withdrawFromAccount() {
		List<Account> accounts = customerService.getActiveAccounts();
		// if active accounts exist, allow a withdrawal
		if(accounts.size() > 0) {
		int accountSelection;
		double withdrawalAmount;
		
		// prompt user to choose an account
		System.out.println("Which account would you like to withdraw from?\n");
		for(int i = 0; i < accounts.size(); i++) {
			System.out.printf("   [%d] %s%n", (i+1), accounts.get(i).getShortDetails());
		}
		//System.out.println();
		
		accountSelection = IOUtil.readMenuChoice(accounts.size());
		
		System.out.printf("%nSelected account: %s%n", accounts.get(accountSelection - 1).getShortDetails());
		
		
		// get the withdrawal amount (max $250,000)
		boolean success;
		String prompt = "Enter the amount you would like to withdraw: ";
		do {
			withdrawalAmount = IOUtil.readDollarAmount(prompt);

			System.out.println();
			
			// attempt a withdrawal
			success = customerService.changeAccountBalance(accounts.get(accountSelection - 1), -withdrawalAmount);
			if(success) {
				System.out.printf("$%,.2f has been withdrawn from account #%d.%n%n", withdrawalAmount, accounts.get(accountSelection - 1).getAccountNo());
				IOUtil.getLogger().info(String.format("ID #: %d, full name: %s - withdrew $%,.2f from account #: %d",
						BankAppSession.getLoggedInUser().getUserID(), BankAppSession.getLoggedInUser().getFullName(), withdrawalAmount, accounts.get(accountSelection - 1).getAccountNo()));
			}
			else {
				System.out.printf("Withdrawal failed: insufficient funds in account #%d.%n%n", accounts.get(accountSelection - 1).getAccountNo());
			}
		} while(!success); // repeat withdrawal process until a correct amount is input
		}
		else {
			System.out.println("User has no active accounts to withdraw from.\n");
		}
		
		IOUtil.promptEnterKey();
	}

	@Override
	public void depositIntoAccount() {
		List<Account> accounts = customerService.getActiveAccounts();
		// if active accounts exist, allow a withdrawal
		if(accounts.size() > 0) {
		int accountSelection;
		double depositAmount;
		
		// prompt user to choose an account
		System.out.println("Which account would you like to deposit into?\n");
		
		for(int i = 0; i < accounts.size(); i++) {
			System.out.printf("   [%d] %s%n", (i+1), accounts.get(i).getShortDetails());
		}
		//System.out.println();
		
		accountSelection = IOUtil.readMenuChoice(accounts.size());
		
		System.out.printf("%nSelected account: %s%n", accounts.get(accountSelection - 1).getShortDetails());

		// get the deposit amount (max $250,000)
		boolean success;
		String prompt = "Enter the amount you would like to deposit: ";
		do {
			depositAmount = IOUtil.readDollarAmount(prompt);
			
			System.out.println();

			// attempt a deposit
			success = customerService.changeAccountBalance(accounts.get(accountSelection - 1), depositAmount);
			if(success) {
				System.out.printf("$%,.2f has been deposited into account #%d.%n%n", depositAmount, accounts.get(accountSelection - 1).getAccountNo());
				IOUtil.getLogger().info(String.format("ID #: %d, full name: %s - deposited $%,.2f into account #: %d",
						BankAppSession.getLoggedInUser().getUserID(), BankAppSession.getLoggedInUser().getFullName(), depositAmount, accounts.get(accountSelection - 1).getAccountNo()));
			}
			else {
				System.out.printf("Deposit failed: account #%d cannot contain more than $250,000.%n%n", accounts.get(accountSelection - 1).getAccountNo());
			}
		} while(!success); // repeat deposit process until a correct amount is input
		}
		else {
			System.out.println("User has no active accounts to deposit into.\n");
		}
		
		IOUtil.promptEnterKey();
	}

}
