package dev.evans.services;

import java.util.List;

import dev.evans.bankapp.BankAppSession;
import dev.evans.daos.CustomerDAO;
import dev.evans.daos.CustomerDAOImpl;
import dev.evans.models.Account;
import dev.evans.models.AccountStatus;
import dev.evans.models.AccountType;
import dev.evans.models.UserRole;
import dev.evans.utils.IOUtil;

public class CustomerServiceImpl implements CustomerService {
	
	private CustomerDAO customerDAO;
	
	// constants for the different account interest rates offered 
	private final double CUS_CHK_INT_RATE = 0.0001;
	private final double EMP_CHK_INT_RATE = 0.0002;
	private final double CUS_SAV_INT_RATE = 0.0005;
	private final double EMP_SAV_INT_RATE = 0.0006;

	public CustomerServiceImpl() {
		super();
		customerDAO = new CustomerDAOImpl();
	}

	@Override
	public boolean addNewPendingAccount(AccountType accountType, double startingBalance) {
		// check if user already has an account pending
		List<Account> existingPendingAccts = customerDAO.getAccountsByIDAndStatus(BankAppSession.getLoggedInUser().getUserID(), AccountStatus.PENDING);
		
		// cancel operation if user already has another pending account awaiting verification
		if(existingPendingAccts.size() > 0) {
			return false;
		}
		
		// else create and initialize the new account
		Account pendingAccount = new Account();
		pendingAccount.setAccountNo(0);
		pendingAccount.setBalance(startingBalance);
		pendingAccount.setAccountType(accountType);
		pendingAccount.setAccountStatus(AccountStatus.PENDING);
		
		// Interest rate is determined by account type and user's role (regular customer, employee)
		double interestRate;
		UserRole userRole = BankAppSession.getLoggedInUser().getUserRole();
		if(accountType == AccountType.CHECKING) { // if account type is checking
			if(userRole == UserRole.CUSTOMER) {
				interestRate = CUS_CHK_INT_RATE; // customer checking interest rate
			}
			else {
				interestRate = EMP_CHK_INT_RATE; // employee checking interest rate
			}
		}
		else { // else account type is savings
			if(userRole == UserRole.CUSTOMER) {
				interestRate = CUS_SAV_INT_RATE; // customer savings interest rate
			}
			else {
				interestRate = EMP_SAV_INT_RATE; // employee savings interest rate
			}
		}
		pendingAccount.setInterestRate(interestRate);
		
		// Add the new account to the database and return the success status
		boolean success = customerDAO.addAccount(pendingAccount, BankAppSession.getLoggedInUser().getUserID());
		
		return success;
	}
	
	@Override
	public boolean changeAccountBalance(Account account, double amountOfChange) {
		if(account == null) { // stop the balance update if the account is null
			return false;
		}
		double oldBalance = account.getBalance();
		double newBalance = oldBalance + amountOfChange;
		// cancel operation if the new account balance goes over the FDIC limit or becomes negative
		if(newBalance > IOUtil.FDIC_LIMIT || newBalance < 0.00) {
			return false;
		}
		// else update the account balance in the database and return the success status
		boolean success = customerDAO.updateAccountBalance(account.getAccountNo(), newBalance);
		
		return success;
	}

	@Override
	public List<Account> getActiveAccounts() {
		return customerDAO.getAccountsByIDAndStatus(BankAppSession.getLoggedInUser().getUserID(), AccountStatus.ACTIVE);
	}

}
