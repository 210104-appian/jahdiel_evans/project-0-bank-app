package dev.evans.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import dev.evans.models.User;
import dev.evans.models.UserRole;
import dev.evans.utils.ConnectionUtil;
import dev.evans.utils.IOUtil;

public class LoginDAOImpl implements LoginDAO {

	public LoginDAOImpl() {
		super();
	}

	@Override
	public User getUserByCredentials(String username, String password) {
		String sql = "SELECT USER_ID, FIRST_NAME, LAST_NAME, USER_ROLE FROM BANK_USER WHERE UPPER(USERNAME) = ? AND PASSWORD = ?";
		User user = null;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setString(1, username);
			pStatement.setString(2, password);
			ResultSet rs = pStatement.executeQuery(); // look for user with matching credentials
			if(rs.next()) { // if a user is returned from the DB, store and return it
				user = new User();
				user.setUserID(rs.getInt("USER_ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				String userRoleString = rs.getString("USER_ROLE");
				boolean isValidRole = Arrays.stream(
						UserRole.values())
						.map((value)->value.toString())
						.anyMatch((str)->str.equals(userRoleString)); // checking to see if the value is found in the enum before parsing
				if(isValidRole) {
					user.setUserRole(UserRole.valueOf(userRoleString));
				}
				user.setAccounts(null); // no need for logged in user to locally store accounts
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to retrieve logged-in user from database. Thrown exception: " + e.getClass());
		}
		return user;
	}

}
