package dev.evans.frontends;

public interface EmployeeFrontend {
	
	public void showEmployeeMenu();
	public void viewUserAccounts();
	public void reviewPendingAccounts();
	public void viewTransactionLog();

}
