package dev.evans.daos;

import java.util.List;

import dev.evans.models.AccountStatus;
import dev.evans.models.User;

public interface EmployeeDAO {
	
	public List<User> getUsersByName(String firstName, String lastName);
	public User getUserByUserID(int userID);
	public List<User> getAllPendingUsers();
	public boolean updateAccountStatus(int accountNo, AccountStatus status);
	public boolean deleteAccount(int accountNo);

}
