package dev.evans.frontends;

public interface CustomerFrontend {
	
	public void showCustomerMenu();
	public void applyForAccount();
	public void viewAccountBalance();
	public void withdrawFromAccount();
	public void depositIntoAccount();

}
