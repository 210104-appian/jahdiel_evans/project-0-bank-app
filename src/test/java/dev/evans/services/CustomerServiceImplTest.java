package dev.evans.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dev.evans.models.Account;

public class CustomerServiceImplTest {
	
	/* JUnit annotations
	 * -----------------
	 * @Test - denotes a test method
	 * @Before - runs before each test
	 * @BeforeClass - runs once before the class runs
	 * @After - runs after each test
	 * @AfterClass - runs once after the entire class runs
	 * @Ignore - skips method with this annotation
	 */
	
	// Testing 6 invalid edge cases and 1 valid case for the changeAccountBalance() method in the CustomerServiceImpl class
	
	private CustomerService custServ = new CustomerServiceImpl();

	@Test
	public void testChangeBalanceWithNullAccount() {
		boolean expected = false;
		boolean actual = custServ.changeAccountBalance(null, 0.00);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChangeBalanceWithInvalidAccountNo() {
		boolean expected = false;
		Account account = new Account();
		account.setAccountNo(0);
		account.setBalance(500.00);
		boolean actual = custServ.changeAccountBalance(account, 0.00);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChangeBalanceWithZeroBalanceAndNegativeChange() {
		boolean expected = false;
		Account account = new Account();
		account.setAccountNo(100000000);
		account.setBalance(0.00);
		boolean actual = custServ.changeAccountBalance(account, -500.00);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChangeBalanceWithMaxBalanceAndPositiveChange() {
		boolean expected = false;
		Account account = new Account();
		account.setAccountNo(100000000);
		account.setBalance(250000.00);
		boolean actual = custServ.changeAccountBalance(account, 500.00);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChangeBalanceWithMaxNegativeChange() {
		boolean expected = false;
		Account account = new Account();
		account.setAccountNo(100000000);
		account.setBalance(1000.00);
		boolean actual = custServ.changeAccountBalance(account, -250000.00);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChangeBalanceWithMaxPositiveChange() {
		boolean expected = false;
		Account account = new Account();
		account.setAccountNo(100000000);
		account.setBalance(1000.00);
		boolean actual = custServ.changeAccountBalance(account, 250000.00);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChangeBalanceWithValidAccount() {
		boolean expected = true;
		Account account = new Account();
		account.setAccountNo(100000000);
		account.setBalance(1000.00);
		boolean actual = custServ.changeAccountBalance(account, 500.00);
		assertEquals(expected, actual);
	}

}
