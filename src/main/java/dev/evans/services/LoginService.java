package dev.evans.services;

import dev.evans.models.UserRole;

public interface LoginService {
	
	public boolean verifyUserCredentials(String username, String password, UserRole userRole);
	public void deleteLoggedInUser();

}
