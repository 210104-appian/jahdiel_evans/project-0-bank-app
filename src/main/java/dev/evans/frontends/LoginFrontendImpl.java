package dev.evans.frontends;

import dev.evans.models.UserRole;
import dev.evans.services.LoginService;
import dev.evans.services.LoginServiceImpl;
import dev.evans.utils.IOUtil;

public class LoginFrontendImpl implements LoginFrontend {
	
	// reference to login service component
	private LoginService loginService;

	public LoginFrontendImpl() {
		super();
		loginService = new LoginServiceImpl();
	}

	@Override
	public int chooseServicePortal() {
		int portalSelection; // stores the user's service portal choice

		IOUtil.clearScreen();

		// display login menu options
		System.out.println("----------------------------------");
		System.out.println("| My Bank User Services Homepage |");
		System.out.println("----------------------------------\n");

		System.out.println("Select a service portal to log into,");
		System.out.println("or select [3] to exit application.\n");
		System.out.println("   [1] Customer Services");
		System.out.println("   [2] Employee Services");
		System.out.println("   [3] Exit Application\n");

		// get a service portal choice from the user
		portalSelection = IOUtil.readMenuChoice(3);

		// Go to the chosen login page or exit app based on user's menu choice
		switch(portalSelection) {
		case 1:
			login(UserRole.CUSTOMER);
			break;
		case 2:
			login(UserRole.EMPLOYEE);
			break;
		case 3:
			IOUtil.clearScreen();
			System.out.println("\nApplication has terminated.");
		}

		return portalSelection;
	}

	@Override
	public void login(UserRole userRole) {
		boolean successfulLogin; // indicates if user logged in successfully
		
		IOUtil.clearScreen();
		
		// display appropriate login page title
		System.out.println("--------------------------------");
		switch(userRole) {
			case CUSTOMER:
				System.out.println("| Customer Services Login Page |");
				break;
			case EMPLOYEE:
				System.out.println("| Employee Services Login Page |");
				
		}
		System.out.println("--------------------------------\n");
		
		do {
			// get username and password from the user
			System.out.print("Please enter your username: ");
			String username = IOUtil.getScanner().nextLine();
			System.out.print("Please enter your password: ");
			String password = IOUtil.getScanner().nextLine();
			
			// verify user's credentials
			successfulLogin = loginService.verifyUserCredentials(username, password, userRole);
			if(!successfulLogin) {
				System.out.println("\nIncorrect username and/or password, please re-enter your credentials.\n");
			}
		} while (!successfulLogin); // repeat while user has entered incorrect credentials
	}

	@Override
	public void logout() {
		loginService.deleteLoggedInUser();
	}

}
