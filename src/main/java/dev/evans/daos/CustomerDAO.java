package dev.evans.daos;

import java.util.List;

import dev.evans.models.Account;
import dev.evans.models.AccountStatus;

public interface CustomerDAO {
	
	public boolean addAccount(Account account, int userID);
	public List<Account> getAccountsByIDAndStatus(int userID, AccountStatus status);
	public boolean updateAccountBalance(int accountNo, double newBalance);

}
