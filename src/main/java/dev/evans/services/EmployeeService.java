package dev.evans.services;

import java.util.List;
import dev.evans.models.User;

public interface EmployeeService {
	
	public List<User> findUsersByName(String firstName, String lastName);
	public User findUserByID(int userID);
	public List<User> getPendingUsers();
	public boolean rejectAccount(int accountNo);
	public boolean approveAccount(int accountNo);

}
