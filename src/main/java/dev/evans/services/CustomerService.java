package dev.evans.services;

import java.util.List;

import dev.evans.models.Account;
import dev.evans.models.AccountType;

public interface CustomerService {

	public boolean addNewPendingAccount(AccountType accountType, double startingBalance);
	public boolean changeAccountBalance(Account account, double amountOfChange);
	public List<Account> getActiveAccounts();
	
}
