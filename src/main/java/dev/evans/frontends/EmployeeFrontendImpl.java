package dev.evans.frontends;

import java.util.ArrayList;
import java.util.List;

import dev.evans.bankapp.BankAppSession;
import dev.evans.models.Account;
import dev.evans.models.User;
import dev.evans.services.EmployeeService;
import dev.evans.services.EmployeeServiceImpl;
import dev.evans.utils.IOUtil;

public class EmployeeFrontendImpl implements EmployeeFrontend {
	
	// reference to employee service component
	private EmployeeService employeeService;

	public EmployeeFrontendImpl() {
		super();
		employeeService = new EmployeeServiceImpl();
	}

	@Override
	public void showEmployeeMenu() {
		boolean isLoggedIn = true; // indicates if the user is logged in
		int selectionInt; // stores the user's numeric menu choice

		do {
			IOUtil.clearScreen();

			// display employee services menu options
			System.out.println("-----------------------------");
			System.out.println("| My Bank Employee Services |");
			System.out.println("-----------------------------\n");

			System.out.println("Logged in user: " + BankAppSession.getLoggedInUser().getFullName() + "\n");

			System.out.println("Please select a banking service to perform,");
			System.out.println("or select [4] to log out.\n");
			System.out.println("   [1] View user accounts");
			System.out.println("   [2] Review pending accounts");
			System.out.println("   [3] View transaction log");
			System.out.println("   [4] Log out\n");

			// get a menu choice from the user
			selectionInt = IOUtil.readMenuChoice(4);

			// clears the screen if a proper menu choice was picked
			if(selectionInt != 0) {
				IOUtil.clearScreen();
			}

			// choice a service based on user's menu choice
			switch(selectionInt) {
			case 1:
				viewUserAccounts();
				break;
			case 2:
				reviewPendingAccounts();
				break;
			case 3:
				viewTransactionLog();
				break;
			case 4:
				isLoggedIn = false;
			}
		} while(isLoggedIn); // re-display menu while user is still logged in
	}

	@Override
	public void viewUserAccounts() {
		
		int searchSelection;
		String firstName = "";
		String lastName = "";
		int userID = 0;
		List<User> userList = new ArrayList<User>();
		User singleUser;
		
		// pick a method to search for a user
		System.out.println("How would you like to search for a user?\n");
		System.out.println("   [1] By first and/or last name");
		System.out.println("   [2] By user ID #\n");
		
		searchSelection = IOUtil.readMenuChoice(2);
		
		System.out.println();
		// search for the user using the selected method
		switch(searchSelection) {
			case 1: // by name
				System.out.print("Enter a first name to search for: ");
				firstName = IOUtil.getScanner().nextLine();
				System.out.print("Enter a last name to search for: ");
				lastName = IOUtil.getScanner().nextLine();
				userList = employeeService.findUsersByName(firstName, lastName);
				break;
			case 2: // by ID
				userID = IOUtil.readIDNumber("Enter the ID # of the user: ");
				singleUser = employeeService.findUserByID(userID);
				if(singleUser != null)
					userList.add(singleUser);
				break;
		}
		// display single user's accounts if list size is 1
		if(userList.size() == 1) {
			System.out.println("\nAccount details for " + userList.get(0).getFullName() + "\n");
			List<Account> accounts = userList.get(0).getAccounts();
			IOUtil.printAccountHeader();
			for(int i = 0; i < accounts.size(); i++) {
				System.out.println(accounts.get(i).getFullDetails());
			}
			//System.out.println();
		}
		// choose from the userList if size is greater than 1
		else if(userList.size() > 1) { 
			System.out.println("\nChoose a user from the list of search results.\n");
			for(int i = 0; i < userList.size(); i++) {
				System.out.printf("   [%d] %s%n", (i+1), userList.get(i).getFullName());
			}
			System.out.println();
			int userSelection = IOUtil.readMenuChoice(userList.size());
			
			System.out.println("\nAccount details for " + userList.get(userSelection - 1).getFullName() + "\n");
			List<Account> accounts = userList.get(userSelection - 1).getAccounts();
			IOUtil.printAccountHeader();
			for(int i = 0; i < accounts.size(); i++) {
				System.out.println(accounts.get(i).getFullDetails());
			}
			//System.out.println();
		}
		// no users were found
		else {
			System.out.println("No users were found.\n");
		}
		
		IOUtil.promptEnterKey();
	}

	@Override
	public void reviewPendingAccounts() {
		List<User> pendingUserList = employeeService.getPendingUsers();
		
		// can't review pending accounts if none exist
		if(pendingUserList.size() == 0) {
			System.out.println("There are currently no users with pending accounts.\n");
		}
		else {
			User user;
			Account pendingAccount;
			int reviewChoice;

			// get the pending account if only 1 user has one
			if(pendingUserList.size() == 1) {
				user = pendingUserList.get(0);
				pendingAccount = user.getAccounts().get(0);
			}
			// choose the pending account by user if multiple pending accounts exist
			else {
				System.out.println("Choose the user whose pending account you would like to review.\n");
				for(int i = 0; i < pendingUserList.size(); i++) {
					System.out.printf("   [%d] %s%n", (i+1), pendingUserList.get(i).getFullName());
				}
				System.out.println();
				int userSelection = IOUtil.readMenuChoice(pendingUserList.size());
				user = pendingUserList.get(userSelection - 1);
				pendingAccount = user.getAccounts().get(0);
			}
			// Have employee choose to approve or reject the pending account
			System.out.println("\nPending account for " + user.getFullName() + "\n");
			IOUtil.printAccountHeader();
			System.out.println(pendingAccount.getFullDetails());
			System.out.println("Would you like to approve or reject the pending account?\n");
			System.out.println("   [1] Approve");
			System.out.println("   [2] Reject\n");
			
			reviewChoice = IOUtil.readMenuChoice(2);
			
			System.out.println();
			
			boolean success;
			
			switch(reviewChoice) {
				case 1:
					
					success = employeeService.approveAccount(pendingAccount.getAccountNo());
					if(success)
						System.out.println("Pending account has been approved.\n");
					else
						System.out.println("Approval failed, try again later.\n");
					break;
				case 2:
					
					success = employeeService.rejectAccount(pendingAccount.getAccountNo());
					if(success)
						System.out.println("Pending account has been rejected.\n");
					else
						System.out.println("Rejection failed, try again later.\n");
			}
			
		}
		
		IOUtil.promptEnterKey();
	}

	@Override
	public void viewTransactionLog() {
		System.out.println("Log of bank transactions performed by users:\n");
		IOUtil.printTransactionLog();
		System.out.println();
		IOUtil.promptEnterKey();
	}

}
