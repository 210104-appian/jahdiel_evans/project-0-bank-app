package dev.evans.bankapp;

import dev.evans.frontends.*;
import dev.evans.models.User;
import dev.evans.utils.IOUtil;

public class BankAppSession {
	
	// references to frontend components
	private LoginFrontend loginFrontend;
	private CustomerFrontend customerFrontend;
	private EmployeeFrontend employeeFrontend;
	
	// stores the currently logged-in user's ID, name, and user role
	private static User loggedInUser;

	public BankAppSession() {
		super();
		loginFrontend = new LoginFrontendImpl();
		customerFrontend = new CustomerFrontendImpl();
		employeeFrontend = new EmployeeFrontendImpl();
		
		loggedInUser = null;
	}
	
	public void startBankAppSession() {
		boolean isRunning = true; // indicates whether or not the bank app session is running
		int servicePortalChoice; // indicates the service portal that was logged into by the user
		do {
			// have the user log in as a customer or employee
			servicePortalChoice = loginFrontend.chooseServicePortal();
			switch(servicePortalChoice) {
			case 1: // user logged in as a customer
				customerFrontend.showCustomerMenu();
				break;
			case 2: // user logged in as an employee
				employeeFrontend.showEmployeeMenu();
				break;
			case 3: // user has exited application
				isRunning = false;
			}
			loginFrontend.logout();
		} while(isRunning);
		IOUtil.getScanner().close(); // close scanner on exit
	}
	
	public static User getLoggedInUser() {
		return loggedInUser;
	}
	
	public static void setLoggedInUser(User user) {
		loggedInUser = user;
	}

}
