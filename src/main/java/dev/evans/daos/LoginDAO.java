package dev.evans.daos;

import dev.evans.models.User;

public interface LoginDAO {

	public User getUserByCredentials(String username, String password);
	
}
