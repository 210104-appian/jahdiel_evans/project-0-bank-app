package dev.evans.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.evans.models.Account;
import dev.evans.models.AccountStatus;
import dev.evans.models.AccountType;
import dev.evans.utils.ConnectionUtil;
import dev.evans.utils.IOUtil;

public class CustomerDAOImpl implements CustomerDAO {

	public CustomerDAOImpl() {
		super();
	}

	@Override
	public boolean addAccount(Account account, int userID) {
		String sql = "INSERT INTO BANK_ACCOUNT (USER_ID, BALANCE, INTEREST_RATE, ACCOUNT_TYPE, ACCOUNT_STATUS) VALUES (?, ?, ?, ?, ?)";
		int numRowsInserted = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setInt(1, userID);
			pStatement.setDouble(2, account.getBalance());
			pStatement.setDouble(3, account.getInterestRate());
			pStatement.setString(4, account.getAccountType().toString());
			pStatement.setString(5, account.getAccountStatus().toString());
			
			// insert a pending account into the DB and get the # of rows inserted
			numRowsInserted = pStatement.executeUpdate();
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to add a pending account to database. Thrown exception: " + e.getClass());
		}
		if(numRowsInserted == 1) { // SQL operation was successful if 1 row was inserted
				return true;
			}
		return false;
	}

	@Override
	public List<Account> getAccountsByIDAndStatus(int userID, AccountStatus status) {
		String sql = "SELECT ACCOUNT_NO, BALANCE, INTEREST_RATE, ACCOUNT_TYPE, ACCOUNT_STATUS FROM BANK_ACCOUNT WHERE USER_ID = ? AND ACCOUNT_STATUS = ?";
		List<Account> acctList = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setInt(1, userID);
			pStatement.setString(2, status.toString());
			
			// get all accounts of a certain status owned by the user
			ResultSet rs = pStatement.executeQuery();
			while(rs.next()) { // add each account from the result set to the list
				Account acct = new Account();
				acct.setAccountNo(rs.getInt("ACCOUNT_NO"));
				acct.setBalance(rs.getDouble("BALANCE"));
				acct.setInterestRate(rs.getDouble("INTEREST_RATE"));
				String acctType = rs.getString("ACCOUNT_TYPE");
				if(acctType != null) {
					acct.setAccountType(AccountType.valueOf(acctType));
				}
				String acctStatus = rs.getString("ACCOUNT_STATUS");
				if(acctStatus != null) {
					acct.setAccountStatus(AccountStatus.valueOf(acctStatus));
				}
				
				acctList.add(acct);
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to retrieve accounts from database. Thrown exception: " + e.getClass());
		}
		return acctList;
	}

	@Override
	public boolean updateAccountBalance(int accountNo, double newBalance) {
		String sql = "UPDATE BANK_ACCOUNT SET BALANCE = ? WHERE ACCOUNT_NO = ?";
		int numRowsUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setDouble(1, newBalance);
			pStatement.setInt(2, accountNo);
			
			// update the account balance in the DB
			numRowsUpdated = pStatement.executeUpdate();
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to update an account balance in database. Thrown exception: " + e.getClass());
		}
		if(numRowsUpdated == 1) { // SQL operation was successful if 1 row was updated
			return true;
		}
		return false;
	}

}
