Project 0 - Bank App
developer: Jahdiel Evans
date: 1/20/2021

Overview:
---------
This Java Maven project is a console-based application that simulates banking operations.
A customer can apply for an account, view their balance, and make withdrawals and deposits.
An employee can approve or deny pending accounts and view account balances for their customers.

Requirements Completed:
-------------------
1. User and Account data is stored in an Oracle database and managed remotely using DBeaver.
2. Data Access is performed through the use of OJDBC in a data layer consisting of Data Access Objects.
3. All input is received using the java.util.Scanner class.
4. Log4j is implemented to log bank transactions to a file.
5. JUnit tests are written to test the functionality of a couple key functions within the application layer.

User Story Features Completed:
------------------------------
1. As a user, I can login.
2. As a customer, I can apply for a new bank account with a starting balance.
3. As a customer, I can view the balance of a specific account.
4. As a customer, I can make a withdrawal or deposit to a specific account.
5. As the system, I reject invalid transactions.
	- Ex:
		- A withdrawal that would result in a negative balance.
		- A deposit or withdrawal of negative money.
6. As an employee, I can approve or reject a pending account application.
7. As an employee, I can view a customer's bank accounts.
8. As an employee, I can view a log of all transactions.

Maven Dependencies:
-------------------
The Bank App was developed in Spring Tools Suite 4 and uses Maven version 4.0.0 to handle its dependencies.
The application will require the following dependencies to be added to Maven's repository in order to run
properly:

- JUnit version 4.12
- OJDBC version 19.8.0.0
- Log4j version 1.2.17

The project also requires JavaSE-1.8 to be set as the compiler source and target. All the
necessary properties and dependencies for the Maven project are provided in the pom.xml file
included in the project's root folder.

How to run:
-----------
To theoretically run the Bank App program in Spring Tool Suite 4:

1. Import the project's root folder into the Package Explorer as a Maven Project
2. Locate the BankAppDriver.java file in the project's src/main/java directory within the
   dev.evans.bankapp package.
3. Right click the BankAppDriver.java file, go to Run As>Run Configurations..., and set up
   the Environment Variables for the BankAppDriver application to include variables for a
   DB_PASSWORD, DB_URL, and DB_USERNAME. This allows for a secure connection to the database 
   without needing to hardcode the database's URL and login credentials.
4. Run BankAppDriver.java as a local Java application.

### NOTE ###
------------
Due to my database's VPC security group, only my computer's IP address is allowed to access my
remote Oracle database, even when the proper environment variables are set up to provide the
Java application with the database's URL and login credentials.
