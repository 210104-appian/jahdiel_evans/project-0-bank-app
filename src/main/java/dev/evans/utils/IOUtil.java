package dev.evans.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class IOUtil {

	// scanner and logger to be used in the bank application
	private static Scanner scanner;
	private static Logger log;
	
	// constant representing the FDIC account balance total limit;
	// used as the max value for any dollar amounts entered as input.
	public static final double FDIC_LIMIT = 250000.00;
	
	public static Scanner getScanner() {
		if(scanner == null) {
			scanner = new Scanner(System.in);
		}
		return scanner;
	}
	
	public static Logger getLogger() {
		if(log == null) {
			log = Logger.getRootLogger();
		}
		return log;
	}
	
	public static void clearScreen() {
		for (int i = 0; i < 20; ++i) System.out.println();
//		System.out.print("\033[H\033[2J");
//		System.out.flush();
	}
	
	public static void promptEnterKey(){
		System.out.print("Press [ENTER] to continue...");
		getScanner().nextLine();
	}
	
	public static void printAccountHeader() {
		String line1 = String.format("%-14s%-14s%-14s%14s%14s%n", "Account Type", "Account #", "Status", "APY", "Balance");
		String line2 = String.format("%70s", " ").replace(' ', '-');
		System.out.println(line1 + line2);
	}
	
//	public static void printAccountHeader() {
//		String line1 = String.format("%9s%18s%18s%18s%18s%n", "Account #", "Account Type", "Status", "APY", "Balance");
//		String line2 = String.format("%81s", " ").replace(' ', '-');
//		System.out.println(line1 + line2);
//	}
	
	public static void printTransactionLog() {
		try(Scanner logScanner = new Scanner(new File("src/main/resources/log.txt"));) {
			while (logScanner.hasNextLine()) {
				String logLine = logScanner.nextLine();
				if(!logLine.contains("ERROR")) {
					System.out.println(logLine);
				}
			}
		}
		catch(FileNotFoundException e) {
			System.out.println("The user transaction log is currently empty.");
			getLogger().error("The log.txt file does not currently exist. Exception thrown: " + e.getClass());
		}
	}
	
	public static int readMenuChoice(int totalNumOfChoices) {
		int selectionInt;
		String selection;
		do {
			System.out.print("> ");

			selection = getScanner().nextLine();
			// check to see if the selection string matches an integer
			if(selection.matches("^\\d+$")) { 
				selectionInt = Integer.parseInt(selection); // parse the string as an integer
				// set selectionInt to 0 if it isn't between 1 and totalNumOfChoices inclusive
				if(selectionInt > totalNumOfChoices || selectionInt == 0) {
					selectionInt = 0;
				}
			}
			// set selectionInt to 0 if an integer was not entered as input
			else {
				selectionInt = 0;
			}

			// Print error message if an invalid menu choice was entered
			if(selectionInt == 0) {
				System.out.println("\nInvalid menu choice, please try again.\n");
			}

		} while(selectionInt == 0); // repeat while user has entered an invalid choice
		
		return selectionInt;
	}
	
	public static int readIDNumber(String prompt) {
		int idNumber;
		String input;
		do {
			System.out.print(prompt); // prompt user
			input = getScanner().nextLine(); // get ID number input string

			// check to see if the ID number input string matches an integer
			if(input.matches("^\\d+$")) { 
				idNumber = Integer.parseInt(input); // parse the string as an integer
			}
			// set idNumber to 0 if an integer was not entered as input
			else {
				idNumber = 0;
			}

			// Print error message if an invalid input was entered
			if(idNumber == 0) {
				System.out.println("\nInvalid input, please enter a positive whole number.\n");
			}

		} while(idNumber == 0); // repeat while user has entered invalid input

		return idNumber;
	}

	
//	public static int readMenuChoice(int totalNumOfChoices) {
//		String selection = getScanner().nextLine();
//		// check to see if the selection string matches an integer
//		if(selection.matches("^\\d+$")) { 
//			int selectionInt = Integer.parseInt(selection); // parse the string as an integer
//			// make sure selectionInt is between 1 and totalNumOfChoices inclusive
//			if(selectionInt <= totalNumOfChoices && selectionInt != 0) {
//				return selectionInt;
//			}
//		}
//		return 0; // 0 represents an invalid selection was entered
//	}
	
	public static double readDollarAmount(String prompt) {
		double dollarAmount;
		String input;
		do {
			System.out.print(prompt); // prompt user

			input = getScanner().nextLine(); // get dollar amount input string from user

			// check to see if the input string matches a positive dollar amount
			if(input.matches("^(\\$)?(([1-9]\\d{0,2}(,\\d{3})*)|(([1-9]\\d*)?\\d))(\\.\\d\\d)?$")) {
				input.replaceAll("[$,]", ""); // strip all $ and , from the input string
				dollarAmount = Double.parseDouble(input); // parse it as a double
				// set dollarAmount to -1.00 if dollar amount is greater than 250000
				if(dollarAmount > FDIC_LIMIT) {
					dollarAmount = -1.00;
				}
			}
			// set dollarAmount to -1.00 if input string was formatted incorrectly
			else {
				dollarAmount = -1.00;
			}

			// Print an error message if an invalid dollar amount was entered
			if(dollarAmount == -1.00) {
				System.out.println("\nInvalid input, please enter a positive dollar amount of $250,000 or less.\n");
			}
		} while(dollarAmount == -1.00); // repeat while user has entered an invalid dollar amount

		return dollarAmount;
	}
	
//	public static double readDollarAmount(double maxAmount) {
//		String input = getScanner().nextLine();
//		// check to see if the input string matches a dollar amount
//		if(input.matches("^(\\$)?(([1-9]\\d{0,2}(,\\d{3})*)|(([1-9]\\d*)?\\d))(\\.\\d\\d)?$")) {
//			input.replaceAll("[$,]", ""); // strip all $ and , from the input string
//			double dollarAmount = Double.parseDouble(input); // parse it as a double
//			// make sure dollarAmount is between 0.00 and maxAmount inclusive
//			if(dollarAmount >= 0.00 && dollarAmount <= maxAmount) {
//				return dollarAmount;
//			}
//		}
//		return -1.00;
//	}
}
