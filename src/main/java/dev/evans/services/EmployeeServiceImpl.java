package dev.evans.services;

import java.util.List;

import dev.evans.daos.EmployeeDAO;
import dev.evans.daos.EmployeeDAOImpl;
import dev.evans.models.AccountStatus;
import dev.evans.models.User;

public class EmployeeServiceImpl implements EmployeeService {
	
	private EmployeeDAO employeeDAO;

	public EmployeeServiceImpl() {
		super();
		employeeDAO = new EmployeeDAOImpl();
	}

	@Override
	public List<User> findUsersByName(String firstName, String lastName) {
		// remove any spaces and convert to upper case for both name search strings
		firstName = firstName.replace(" ", "").toUpperCase();
		lastName = lastName.replace(" ", "").toUpperCase();
		// return the users from the database that match the first and/or last name
		return employeeDAO.getUsersByName(firstName, lastName);
	}
	
	@Override
	public User findUserByID(int userID) {
		return employeeDAO.getUserByUserID(userID);
	}	

	@Override
	public List<User> getPendingUsers() {
		return employeeDAO.getAllPendingUsers();
	}

	@Override
	public boolean rejectAccount(int accountNo) {
		boolean success = employeeDAO.deleteAccount(accountNo);
		return success;
	}

	@Override
	public boolean approveAccount(int accountNo) {
		boolean success = employeeDAO.updateAccountStatus(accountNo, AccountStatus.ACTIVE);
		return success;
	}

}
