package dev.evans.frontends;

import dev.evans.models.UserRole;

public interface LoginFrontend {
	
	public int chooseServicePortal();
	public void login(UserRole userRole);
	public void logout();

}
