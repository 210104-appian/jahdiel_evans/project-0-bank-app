package dev.evans.models;

public class Account {
	
	private int accountNo;
	private double balance;
	private double interestRate;
	private AccountType accountType;
	private AccountStatus accountStatus;

	public Account() {
		super();
	}

	public int getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public AccountStatus getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(AccountStatus accountStatus) {
		this.accountStatus = accountStatus;
	}
	
	public String getShortDetails() {
		String moneyFormat = String.format("$%,.2f", balance);
		String details = String.format("%-14s#%-13d%14s%n", accountType.toString(), accountNo, moneyFormat);
		return details;
	}
	
	public String getFullDetails() {
		String moneyFormat = String.format("$%,.2f", balance);
		String details = String.format("%-14s%-14d%-14s%13.2f%%%14s%n", accountType.toString(), accountNo, accountStatus.toString(), interestRate * 100, moneyFormat);
		return details;
	}

}
