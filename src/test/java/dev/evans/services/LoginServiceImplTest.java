package dev.evans.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dev.evans.models.UserRole;

public class LoginServiceImplTest {

	LoginService loginServ = new LoginServiceImpl();
	
	// Testing 5 invalid edge cases and 3 valid cases for the verifyUserCredentials() method in the LoginServiceImpl class
	
	@Test
	public void testVerifyUserCredWithNullValues() {
		boolean expected = false;
		boolean actual = loginServ.verifyUserCredentials(null, null, null);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserCredWithEmptyStrings() {
		boolean expected = false;
		boolean actual = loginServ.verifyUserCredentials("", "", UserRole.CUSTOMER);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserCredWithAllWrongCreds() {
		boolean expected = false;
		boolean actual = loginServ.verifyUserCredentials("Bobby23", "P4ssW0rd", UserRole.CUSTOMER);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserCredWithWrongPassword() {
		boolean expected = false;
		boolean actual = loginServ.verifyUserCredentials("BensonW12", "P4ssW0rd", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserCredWithCustomerLoggingInAsEmployee() {
		boolean expected = false;
		boolean actual = loginServ.verifyUserCredentials("PerezJ84", "$981OwlGal", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserCredWithEmployeeLoggingInAsCustomer() {
		boolean expected = true;
		boolean actual = loginServ.verifyUserCredentials("KanekoS95", "55&ILuvDog$", UserRole.CUSTOMER);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserCredWithCaseInsensitveUsername() {
		boolean expected = true;
		boolean actual = loginServ.verifyUserCredentials("oneilw67", "Willy%4893", UserRole.CUSTOMER);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserCredWithValidCreds() {
		boolean expected = true;
		boolean actual = loginServ.verifyUserCredentials("ParkerY43", "28Yassy$37", UserRole.CUSTOMER);
		assertEquals(expected, actual);
	}

}
