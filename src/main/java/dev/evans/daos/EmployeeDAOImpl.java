package dev.evans.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dev.evans.models.Account;
import dev.evans.models.AccountStatus;
import dev.evans.models.AccountType;
import dev.evans.models.User;
import dev.evans.models.UserRole;
import dev.evans.utils.ConnectionUtil;
import dev.evans.utils.IOUtil;

public class EmployeeDAOImpl implements EmployeeDAO {

	public EmployeeDAOImpl() {
		super();
	}

	@Override
	public List<User> getUsersByName(String firstName, String lastName) {
		List<User> searchedUsers = new ArrayList<>();
		String sql = "SELECT * " +
				"FROM  { oj BANK_USER bu " +
				"LEFT OUTER JOIN BANK_ACCOUNT ba " +
				"ON bu.USER_ID = ba.USER_ID } " +
				"WHERE UPPER(bu.FIRST_NAME) LIKE ? OR UPPER(bu.LAST_NAME) LIKE ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setString(1, "%"+firstName+"%");
			pStatement.setString(2, "%"+lastName+"%");
			ResultSet rs = pStatement.executeQuery();
			// get all users whose first and/or last name matches the search criteria
			searchedUsers = processUserResultSet(rs);
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to retrieve users from database. Thrown exception: " + e.getClass());
		}
		return searchedUsers;
	}

	@Override
	public User getUserByUserID(int userID) {
		List<User> users = new ArrayList<>();
		String sql = "SELECT * " +
				"FROM  { oj BANK_USER bu " +
				"LEFT OUTER JOIN BANK_ACCOUNT ba " +
				"ON bu.USER_ID = ba.USER_ID } " +
				"WHERE bu.USER_ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setInt(1, userID);
			ResultSet rs = pStatement.executeQuery();
			users = processUserResultSet(rs);
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to retrieve a user from database. Thrown exception: " + e.getClass());
		}
		if(users.size() > 0) {
			return users.get(0);
		}
		return null;
	}

	@Override
	public List<User> getAllPendingUsers() {
		List<User> pendingUsers = new ArrayList<>();
		String sql = "SELECT * " +
				"FROM  { oj BANK_USER bu " +
				"LEFT OUTER JOIN BANK_ACCOUNT ba " +
				"ON bu.USER_ID = ba.USER_ID } " +
				"WHERE ba.ACCOUNT_STATUS = ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setString(1, AccountStatus.PENDING.toString());
			ResultSet rs = pStatement.executeQuery();
			pendingUsers = processUserResultSet(rs);
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to retrieve pending users from database. Thrown exception: " + e.getClass());
		}
		return pendingUsers;
	}
	
	private List<User> processUserResultSet(ResultSet rs) throws SQLException{
		List<User> users = new ArrayList<>();
		while(rs.next()) { // for each record
			int userID = rs.getInt(1); // first column is USER_ID
			boolean existsUser = false;
			for(User user: users) {
				if(userID==user.getUserID()) {
					existsUser = true;

					// if the user already exists, we add the account to the existing user's account list
					Account acct = accountFactory(rs);
					user.getAccounts().add(acct);

					break; // stop checking the user IDs if we've already found one that matches
				} 
			}
			if(!existsUser) {
				// create new user if it did not already exist
				User user = userFactory(rs);
				Account acct = accountFactory(rs);
				if(acct!=null) {
					user.getAccounts().add(acct);
				}

				users.add(user);	
			}
		}
		return users;
	}
	
	private User userFactory(ResultSet rs) throws SQLException {
		User user = new User();
		user.setUserID(rs.getInt(1)); // first column is USER_ID
		user.setFirstName(rs.getString("FIRST_NAME"));
		user.setLastName(rs.getString("LAST_NAME"));
		String roleString = rs.getString("USER_ROLE");
		boolean isValidRole = Arrays.stream(
				UserRole.values())
				.map((value)->value.toString())
				.anyMatch((str)->str.equals(roleString)); // checking to see if the value is found in the enum before parsing
		if(isValidRole) {
			user.setUserRole(UserRole.valueOf(roleString));
		}
		user.setAccounts(new ArrayList<>());

		return user;
	}
	
	private Account accountFactory(ResultSet rs) throws SQLException {
		int accountNo= rs.getInt("ACCOUNT_NO");
		if(accountNo != 0) { //this can be a 0 value if the user has no accounts (because this is a left join)
				Account acct = new Account();
				acct.setAccountNo(rs.getInt("ACCOUNT_NO"));
				acct.setBalance(rs.getDouble("BALANCE"));
				acct.setInterestRate(rs.getDouble("INTEREST_RATE"));
				String accountType = rs.getString("ACCOUNT_TYPE");
				if(accountType!=null) {
					AccountType type = AccountType.valueOf(accountType);
					acct.setAccountType(type);
				}
				String accountStatus = rs.getString("ACCOUNT_STATUS");
				if(accountStatus!=null) {
					AccountStatus status = AccountStatus.valueOf(accountStatus);
					acct.setAccountStatus(status);
				}
				
				return acct;
		}
		return null;
	}

	@Override
	public boolean updateAccountStatus(int accountNo, AccountStatus status) {
		String sql = "UPDATE BANK_ACCOUNT SET ACCOUNT_STATUS = ? WHERE ACCOUNT_NO = ?";
		int numRowsUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setString(1, status.toString());
			pStatement.setInt(2, accountNo);
			
			// update the account status in the DB
			numRowsUpdated = pStatement.executeUpdate();
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to update an account status in database. Thrown exception: " + e.getClass());
		}
		if(numRowsUpdated == 1) { // SQL operation was successful if 1 row was updated
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteAccount(int accountNo) {
		String sql = "DELETE FROM BANK_ACCOUNT WHERE ACCOUNT_NO = ?";
		int numRowsDeleted = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setInt(1, accountNo);
			
			// delete the specified account from the DB
			numRowsDeleted = pStatement.executeUpdate();
		}
		catch(SQLException e) {
			e.printStackTrace();
			IOUtil.getLogger().error("Unable to delete an account from database. Thrown exception: " + e.getClass());
		}
		if(numRowsDeleted == 1) { // SQL operation was successful if 1 row was deleted
			return true;
		}
		return false;
	}

}
